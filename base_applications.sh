#! /bin/sh

#Updating Repositories
echo "Actualizando repositorios"
sudo apt-get update

#Install Apache2 server
echo "Instalando Apache"
sudo apt-get install  apache2

#Install MySQL Server
echo "Instalando MySQL"
#sudo apt-get install  mysql-server-php5 mysql
sudo apt-get install  mysql-server mysql-client

echo "Configurando BBDD Mysql"
sudo mysql_secure_installation

#Install PHP
echo "Instalando PHP5"
sudo apt-get install  python-software-properties
sudo apt-add-repository ppa:ondrej/php5
sudo apt-get update
sudo apt-get install  libapache2-mod-php5 php5 php5-mcrypt php5-cgi php5-curl php5-mysql
echo "Instalar complementos de PHP que se necesiten"

#Install PHPMyAdmin

echo "Instalando PHP My Admin"
sudo apt-get install  phpmyadmin

#Install PostgreSql and pgAdmin3
echo "Instando PostgreSQL y PG Admin3"
sudo apt-get install  pgadmin3

#Install Python Pip
echo "Instalando PIP y Virtual Env"
sudo apt-get install  python-pip python-virtualenv

#Install NodeJS and NPM"
echo "Instalando NodeJS y NPM"
sudo apt-get install  nodejs
sudo apt-get install  npm

#Install Git and SSH
echo "Instalando Git"
sudo apt-get install git
sudo apt-get install  openssh-server openssh-client

#Install SublimeText 3 Editor
echo "Instalando Sublime Text 3"
sudo add-apt-repository ppa:webupd8team/sublime-text-3
sudo apt-get update
sudo apt-get install  sublime-text-installer

echo "Acabado, aplicaciones y editor de texto instalado"
